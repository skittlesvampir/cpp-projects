#include <iostream>
#include <cstdlib>
#include <cstdio>
using namespace std;

int main(){
	int nInput, nInputO;
	int i, ii, iii;
	cin >> nInputO; //O stand for original
	nInput = nInputO;
	int PrimeCount=0;
//getting primes
	int PrimesAll[nInput];
	PrimesAll[0]=0;
	PrimesAll[1]=0;

	//filling array with numbers
	for(i=2;i<=nInputO;i++){
		PrimesAll[i]=i;
	}
	//replacing numbers that aren't primes with 0's
	for(i=2;i<=nInputO;i++){
		if(PrimesAll[i]!=0){
			for(iii=PrimesAll[i], ii=PrimesAll[i];ii<=nInput;ii=ii+iii){
				if(ii>PrimesAll[i]){
					PrimesAll[ii]=0;
				}else{
					PrimeCount++;
				//	cout << PrimesAll[ii];
				}
			}
		}
	}
	//filtering the 0's
	int Primes[PrimeCount];
	int ArrayPos=0;
	for(i=2;i<=nInput;i++){
		if(PrimesAll[i]!=0){
			Primes[ArrayPos]=PrimesAll[i];
			ArrayPos++;

		}
	}
	//
	bool finished=false;
	while(finished==false){
		for(i=0;i<=PrimeCount;i++){
			if(nInput%Primes[i]==0){
				if(nInput<nInputO){
					cout << "x" << Primes[i];
				}else{
					cout << Primes[i];
				}
				nInput=nInput/Primes[i];
				break;
			}
		}
		if(nInput==1){
			finished=true;
			cout << endl;
	}
	}


}

