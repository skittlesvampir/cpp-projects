# Prime Numbers
In this folder are my projects about prime numbers.
* [_prime-finder.cpp_](https://gitlab.com/skittlesvampir/cpp-projects/-/blob/master/primes/prime-finder.cpp) is program that lists all prime numbers from 2 to a number you entered.
* [_primefactorizator.cpp_](https://gitlab.com/skittlesvampir/cpp-projects/-/blob/master/primes/primefactorizator.cpp) is a program that breaks a number you entered into its prime factors.
